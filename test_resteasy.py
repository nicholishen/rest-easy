import unittest
from resteasy import RestEasy
import requests_mock
import requests
import json


class TestRest(unittest.TestCase):

    BASE_URL = 'http://test.com'
    PATH = '/sample/path/to/resource'
    URL = BASE_URL + PATH

    def test_mock(self):
        with requests_mock.mock() as m:
            m.get(self.BASE_URL, text="testing")
            r = requests.get(self.BASE_URL)
            self.assertEqual('testing', r.text)

    def test_methods(self):
        client = RestEasy(self.BASE_URL)
        for method in ['GET', 'POST', 'PUT', 'DELETE', 'PATCH']:
            with requests_mock.mock() as m:
                meth = getattr(m, method.lower())
                meth(self.BASE_URL, text=method)
                request = getattr(client, method)
                response = request(return_response_ez=True)
                self.assertEqual(response.text, method)

    def test_urls(self):
        client = RestEasy(self.BASE_URL)
        test_urls = [
            client.sample.path.to.resource.ez_url,
            client.sample('sample').path.to('to').resource.ez_url,
            client.placeHOLDER('sample').p('path').t('to').r('resource').ez_url
        ]
        for url in test_urls:
            self.assertEqual(url, self.URL)

    def test_append_slash_option(self):
        client = RestEasy(
            self.BASE_URL, append_slash_ez=True).sample.path.to.resource
        self.assertEqual(client.ez_url, self.URL + '/')

    def test_json(self):
        resp = {
            "data": {
                "array": [
                    {
                        "name": "test json",
                        "number": 1
                    }, {
                        "name": "test json2",
                        "number": 2
                    }
                ],
                "nested": "nested comment"
            }
        }
        client = RestEasy(self.BASE_URL)
        with requests_mock.mock() as m:
            m.get(self.BASE_URL, json=resp, headers={'content-type': 'json'})
            self.assertEqual(
                resp['data']['array'][0]['number'],
                client.GET().data.array[0].number
            )

    def test_xml(self):
        resp = '''
            <note>
                <from>Jani</from>
                <to>Tove</to>
                <message>Remember me this weekend</message>
            </note>
        '''
        client = RestEasy(self.BASE_URL)
        with requests_mock.mock() as m:
            m.get(self.BASE_URL, text=resp, headers={'content-type': 'xml'})
            self.assertEqual(
                'Tove',
                client.GET().note.to
            )


if __name__ == '__main__':
    unittest.main()
