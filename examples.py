# # from resteasy import *
# import pprint
# from hammock import Hammock
# import xmltodict
# # api = RestEasy({
# #     'base_url':'http://api.com/api',
# #     'params':{
# #         'access_tocken': 'skdhfo8ashdfhsalkjdhflkajs',
# #         'limit':100,
# #     }
# # })
# # #formulation of endpoint with a 'url' which is a class method 
# # #and and invalid variable name '3-6'
# # url = api.v3.python.url_('url').version('3-6').cool.foo.bar.params(foo='bar')
# # print(url)

# # ##############################################################################
# # #EXAMPLE 1
# # bitly_api = RestEasy({
# #     'base_url': 'https://api-ssl.bitly.com/v3',
# #     'params': {     #global level params
# #         'access_token': 'YourAccessTokenGoesHere',
# #         'format': 'json',
# #     }
# # })
# # long_url = 'http://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html'
# # short_url = bitly_api.shorten.do_request(longUrl=long_url).data.url
# # whois_op = bitly_api.info.do_request(shortUrl=short_url).data.info[0].created_by

# # print('shortened url:', short_url)
# # print('...but who is OP?\nOP:', whois_op)

# # ##############################################################################
# #EXAMPLE 2
# # re_api = RestEasy({'base_url': 'https://api.iextrading.com/1.0'})
# # #endpoint GET /stock/{symbol}/ohlc
# # open_price = re_api.stock.symbol('aapl').ohlc.do_request().open.price
# # print(open_price)

# # ##############################################################################
# # #EXAMPLE 3
# # bitfinex = RestEasy(config={'base_url': 'https://api.bitfinex.com/v2'})

# # btc_book = bitfinex.book.tBTCUSD.P0.params({'len': 25})
# # print(btc_book.url)

# # btc_ticker = bitfinex.ticker.tBTCUSD

# # btc_ticker.do_request()
# # for book_event in btc_ticker.response_gen():
# #     print(book_event)

# # ##############################################################################

# # api = RestEasy('https://api.bitfinex.com/v2')

# # symbols = ['tBTCUSD', 'tLTCUSD']
# # for symbol in api.ticker.symbols('tBTCUSD', 'tLTCUSD'):
# #     print(symbol.GET(return_type=ReturnType.AUTO))
# # ##############################################################################
# bitly_api = RestEasy(
#     base_url_ez='https://api-ssl.bitly.com/v3',
#     append_slash_ez=False,
#     return_response_ez=False,
#     params_ez={
#         'access_token': '04a37a47cf5d9ee42e779b7dc886fd0d969eda99',
#         'format': 'xml',
#     },
#     headers_ez={
#         'blah': 'header-stuff'
#     }
# )

# long_url = 'http://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html'
# short_url = bitly_api.shorten.GET(longUrl=long_url).data.url

# whois_op = bitly_api.info.GET(shortUrl=short_url).data.info.created_by
# print('shortened url:', short_url)
# print('...but who is OP?\nOP:', whois_op)
# # ##############################################################################


# resp = """<Capabilities xmlns="http://www.opengis.net/cat/csw" version="2.0.0" updateSequence="0" xmlns:ows="http://www.opengis.net/ows" xmlns:ogc="http://www.opengis.net/ogc" xmlns:csw="http://www.opengis.net/cat/csw" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:ns0="http://www.opengis.net/cat/csw" xmlns:ns1="http://www.opengis.net/cat/csw">
#   <ows:ServiceIdentification xmlns:ows="http://www.opengis.net/ows">
#     <ows:ServiceType>CSW</ows:ServiceType>
#     <ows:ServiceTypeVersion>2.0.0</ows:ServiceTypeVersion>
#     <ows:Title>Company CSW</ows:Title>
#     <ows:Abstract>
#          A catalogue service that conforms to the HTTP protocol
#          binding of the OpenGIS Catalogue Service specification
#          version 2.0.0.
#       </ows:Abstract>
#     <ows:Keywords>
#       <ows:Keyword>CSW</ows:Keyword>
#       <ows:Keyword>Company Name</ows:Keyword>
#       <ows:Keyword>geospatial</ows:Keyword>
#       <ows:Keyword>catalogue</ows:Keyword>
#     </ows:Keywords>
#     <ows:Fees>NONE</ows:Fees>
#     <ows:AccessConstraints>NONE</ows:AccessConstraints>
#   </ows:ServiceIdentification>
#   <ows:ServiceProvider xmlns:ows="http://www.opengis.net/ows">
#     <ows:ProviderName>Company Name</ows:ProviderName>
#     <ows:ProviderSite ans1:href="http://www.oracle.com" xmlns:ans1="http://www.w3.org/1999/xlink"/>
#     <ows:ServiceContact>
#       <ows:IndividualName> Contact Person Name</ows:IndividualName>
#       <ows:PositionName>Staff</ows:PositionName>
#       <ows:ContactInfo>
#         <ows:Phone>
#           <ows:Voice>999-999-9999</ows:Voice>
#           <ows:Facsimile>999-999-9999</ows:Facsimile>
#         </ows:Phone>
#         <ows:Address>
#           <ows:DeliveryPoint>1 Street Name</ows:DeliveryPoint>
#           <ows:City>CityName</ows:City>
#           <ows:AdministrativeArea>StateName</ows:AdministrativeArea>
#           <ows:PostalCode>09999</ows:PostalCode>
#           <ows:Country>USA</ows:Country>
#           <ows:ElectronicMailAddress>
#                contact.person@example.com
#                </ows:ElectronicMailAddress>
#         </ows:Address>
#         <ows:OnlineResource ans1:href="mailto:contact.person@example.com" xmlns:ans1="http://www.w3.org/1999/xlink"/>
#       </ows:ContactInfo>
#     </ows:ServiceContact>
#   </ows:ServiceProvider>
#   <ows:OperationsMetadata xmlns:ows="http://www.opengis.net/ows">
#     <ows:Operation name="GetCapabilities">
#       <ows:DCP>
#         <ows:HTTP>
#           <ows:Get ans1:href="http://localhost:8888/SpatialWS-SpatialWS-context-root/cswservlet" xmlns:ans1="http://www.w3.org/1999/xlink"/>
#           <ows:Post ans1:href="http://localhost:8888/SpatialWS-SpatialWS-context-root/SpatialWSSoapHttpPort" xmlns:ans1="http://www.w3.org/1999/xlink"/>
#         </ows:HTTP>
#       </ows:DCP>
#     </ows:Operation>
#     <ows:Operation name="DescribeRecord">
#       <ows:DCP>
#         <ows:HTTP>
#           <ows:Post ans1:href="http://localhost:8888/SpatialWS-SpatialWS-context-root/SpatialWSSoapHttpPort" xmlns:ans1="http://www.w3.org/1999/xlink"/>
#         </ows:HTTP>
#       </ows:DCP>
#       <ows:Parameter name="typeName"><ows:Value>ns0:SampleRecord</ows:Value><ows:Value>ns1:Record</ows:Value></ows:Parameter>
#       <ows:Parameter name="outputFormat">
#         <ows:Value>text/xml</ows:Value>
#       </ows:Parameter>
#       <ows:Parameter name="schemaLanguage">
#         <ows:Value>XMLSCHEMA</ows:Value>
#       </ows:Parameter>
#     </ows:Operation>
#     <ows:Operation name="GetRecords">
#       <ows:DCP>
#         <ows:HTTP>
#           <ows:Post ans1:href="http://localhost:8888/SpatialWS-SpatialWS-context-root/SpatialWSSoapHttpPort" xmlns:ans1="http://www.w3.org/1999/xlink"/>
#         </ows:HTTP>
#       </ows:DCP>
#       <ows:Parameter name="TypeName"><ows:Value>ns0:SampleRecord</ows:Value><ows:Value>ns1:Record</ows:Value></ows:Parameter>
#       <ows:Parameter name="outputFormat">
#         <ows:Value>text/xml </ows:Value>
#       </ows:Parameter>
#       <ows:Parameter name="outputSchema">
#         <ows:Value>OGCCORE</ows:Value>
#       </ows:Parameter>
#       <ows:Parameter name="resultType">
#         <ows:Value>hits</ows:Value>
#         <ows:Value>results</ows:Value>
#         <ows:Value>validate</ows:Value>
#       </ows:Parameter>
#       <ows:Parameter name="ElementSetName">
#         <ows:Value>brief</ows:Value>
#         <ows:Value>summary</ows:Value>
#         <ows:Value>full</ows:Value>
#       </ows:Parameter>
#       <ows:Parameter name="CONSTRAINTLANGUAGE">
#         <ows:Value>Filter</ows:Value>
#       </ows:Parameter>
#     </ows:Operation>
#     <ows:Operation name="GetRecordById">
#       <ows:DCP>
#         <ows:HTTP>
#           <ows:Post ans1:href="http://localhost:8888/SpatialWS-SpatialWS-context-root/SpatialWSSoapHttpPort" xmlns:ans1="http://www.w3.org/1999/xlink"/>
#         </ows:HTTP>
#       </ows:DCP>
#       <ows:Parameter name="ElementSetName">
#         <ows:Value>brief</ows:Value>
#         <ows:Value>summary</ows:Value>
#         <ows:Value>full</ows:Value>
#       </ows:Parameter>
#     </ows:Operation>
#     <ows:Operation name="GetDomain">
#       <ows:DCP>
#         <ows:HTTP>
#           <ows:Post ans1:href="http://localhost:8888/SpatialWS-SpatialWS-context-root/SpatialWSSoapHttpPort" xmlns:ans1="http://www.w3.org/1999/xlink"/>
#         </ows:HTTP>
#       </ows:DCP>
#       <ows:Parameter name="ParameterName">
#         <ows:Value>GetRecords.resultType</ows:Value>
#         <ows:Value>GetRecords.outputFormat</ows:Value>
#         <ows:Value>GetRecords.outputRecType</ows:Value>
#         <ows:Value>GetRecords.typeNames</ows:Value>
#         <ows:Value>GetRecords.ElementSetName</ows:Value>
#         <ows:Value>GetRecords.ElementName</ows:Value>
#         <ows:Value>GetRecords.CONSTRAINTLANGUAGE</ows:Value>
#         <ows:Value>GetRecordById.ElementSetName</ows:Value>
#         <ows:Value>DescribeRecord.typeName</ows:Value>
#         <ows:Value>DescribeRecord.schemaLanguage</ows:Value>
#       </ows:Parameter>
#     </ows:Operation>
#     <ows:Operation name="Transaction">
#       <ows:DCP>
#         <ows:HTTP>
#           <ows:Post ans1:href="http://localhost:8888/SpatialWS-SpatialWS-context-root/SpatialWSSoapHttpPort" xmlns:ans1="http://www.w3.org/1999/xlink"/>
#         </ows:HTTP>
#       </ows:DCP>
#     </ows:Operation>
#     <ows:Parameter name="service">
#       <ows:Value>CSW</ows:Value>
#     </ows:Parameter>
#     <ows:Parameter name="version">
#       <ows:Value>2.0.0</ows:Value>
#     </ows:Parameter>
#     <ows:ExtendedCapabilities>
#       <ogc:Filter_Capabilities xmlns:ogc="http://www.opengis.net/ogc">
#         <ogc:Spatial_Capabilities>
#           <ogc:Spatial_Operators>
#             <ogc:BBOX/>
#             <ogc:Equals/>
#             <ogc:Disjoint/>
#             <ogc:Intersect/>
#             <ogc:Touches/>
#             <ogc:Crosses/>
#             <ogc:Within/>
#             <ogc:Contains/>
#             <ogc:Overlaps/>
#             <ogc:Beyond/>
#             <ogc:DWithin/>
#           </ogc:Spatial_Operators>
#         </ogc:Spatial_Capabilities>
#         <ogc:Scalar_Capabilities>
#           <ogc:Logical_Operators/>
#           <ogc:Comparison_Operators>
#             <ogc:Simple_Comparisons/>
#             <ogc:Like/>
#             <ogc:Between/>
#             <ogc:NullCheck/>
#           </ogc:Comparison_Operators>
#           <ogc:Arithmetic_Operators>
#             <ogc:Simple_Arithmetic/>
#           </ogc:Arithmetic_Operators>
#         </ogc:Scalar_Capabilities>
#       </ogc:Filter_Capabilities>
#     </ows:ExtendedCapabilities>
#   </ows:OperationsMetadata>
# </Capabilities>"""

# from collections import OrderedDict
# from attrdict import AttrDict

# x = xmltodict.parse(resp)


# def convert_attrdict(d):
#     d = AttrDict(d)
#     for k, v in d.items():
#         if isinstance(v, OrderedDict):
#             d[k] = AttrDict(v)
#             d = convert_attrdict(v)
#     return d

# x = convert_attrdict(x)

# pprint.pprint(x)
