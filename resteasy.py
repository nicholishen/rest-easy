# -*- coding: utf-8 -*-
"""
a generic REST API client. 

RestEasy is a generic multi-purpose REST API client that
combines the magic of python with the convenience of AttrDict
to make REST API interactions a breeze. 

Example:
    # non-parametric endpoints
    from resteasy import RestEasy

    bitly_api = RestEasy(
        'base_url': 'https://api-ssl.bitly.com/v3',
        'params': {  
            'access_token': 'YourAccessTokenGoesHere',
            'format': 'json',
        }
    })

    long_url  = 'http://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html'
    short_url = bitly_api.shorten.do_request(longUrl=long_url).data.url
    whois_op  = bitly_api.info.do_request(shortUrl=short_url).data.info[0].created_by

    print('shortened url:', short_url)
    print('...but who is OP?\nOP is', whois_op)

Example:
    # parametric endpoints

TODO:
    * Make better docs!
    * ???
"""

from attrdict import AttrDict
from collections import OrderedDict
from requests import Request, Session
import xmltodict


__all__ = ['RestEasy', 'ResponseReturnError']


class ResponseReturnError(Exception):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class RestEasy:

    HTTP_METHODS = ['get', 'options', 'head', 'post', 'put', 'patch', 'delete']

    def __init__(self, base_url_ez: str=None, **kw):
        """
        """
        self._session   = None
        self._params    = {}
        self._children  = {}
        self._parent    = kw.pop('parent', None)
        if self._parent is not None:
            self._return_reponse = self._parent._return_reponse
            self._append_slash   = self._parent._append_slash
        else:
            self._return_reponse = kw.pop('return_reponse_ez', False)
            self._append_slash   = kw.pop('append_slash_ez', False)
        self._endpoint = kw.pop('endpoint', None)
        if self._parent is None:
            self.__toplevel_setup(base_url_ez, **kw)

    def __toplevel_setup(self, base_url_ez, **kw):
        """
        """
        if base_url_ez is None or not isinstance(base_url_ez, str):
            raise ValueError('Base url not specified or not in str.')
        self._url = base_url_ez
        self._session = Session()
        params_ez = kw.pop('params_ez', {})
        if not isinstance(params_ez, dict):
            raise ValueError('Params must be in dict.')
        self._params = params_ez
        headers = kw.pop('headers_ez', {})
        if not isinstance(headers, dict):
            raise ValueError('Headers must be in a dict.')
        self._session.headers = headers
        # self._return_reponse = kw.pop('return_response', False)

    def _get_url(self):
        """
        """
        if self._parent is not None:
            return f'{self._parent._get_url()}/{self._endpoint}'
        else:
            return self._url

    def __getattr__(self, key):
        """
        """
        if key not in self._children:
            self._children.update({
                key: RestEasy(
                    parent=self,
                    endpoint=key,
                )
            })
        return self._children[key]

    def __call__(self, arg=None, *args, **kw):
        """
        """
        return_response = kw.pop('return_response_ez', None)
        if return_response is not None:
            self._return_reponse = return_response
        if kw:
            self.ez_params(**kw)
        if isinstance(arg, list):
            return self.__endpoints(arg)
        if isinstance(arg, str):
            if len(args) == 0:
                self._endpoint = f'{arg}'
            else:
                return self.__endpoints([arg] + [a for a in args if isinstance(a, str)])
        return self

    def __endpoints(self, add_endpoints: list=None):
        for ep in add_endpoints:
            yield self(ep)

    def __repr__(self):
        return f'<RestEasy {self._get_url()}'

    @property
    def _get_return_response(self) -> bool:
        return self._return_reponse

    @property
    def ez_url(self):
        """
        """
        url  = self._get_url() + ('/' if self._append_slash else '')
        req  = Request(url=url, params=self.ez_params())
        prep = req.prepare()
        return prep.url

    @property
    def ez_session(self):
        """
        """
        if self._parent is not None:
            return self._parent.ez_session
        return self._session

    @property
    def ez_response(self):
        return self._last_response

    def ez_params(self, clear=False, **params):
        """
        """
        if clear:
            self._params.clear()
        if params:
            p = {k: v for k, v in params.items() if not k.endswith('_ez')}
            self._params.update(p)
            return self
        if self._parent is not None:
            self._params.update(self._parent.ez_params())
        return self._params


def bind_method(method):
    """Bind `requests` module HTTP verbs to `RestEasy` class as
    static methods. Borrowed from Hammock!"""

    def do_request(self, return_response_ez=None, **request_params):
        """
        """
        url = self._get_url()
        params = {**self.ez_params(), **request_params}
        session = self.ez_session
        r = self._last_response = session.request(
            method, url=url, params=params
        )

        if (return_response_ez or self._get_return_response):
            return r

        if 'content-type' in r.headers:
            if 'json' in r.headers['content-type']:
                res = r.json()
                if isinstance(res, dict):
                    return AttrDict(r.json())
                else:
                    return r.json()
            if 'xml' in r.headers['content-type']:
                x = xmltodict.parse(r.text)
                if isinstance(x, list):
                    return x

                def convert(d):
                    d = AttrDict(d)
                    for k, v in d.items():
                        if isinstance(v, OrderedDict):
                            d[k] = convert(v)
                    return d
                x = convert(x)
                if 'response' in x:
                    x = x.response
                return x
        raise ResponseReturnError(
            'Content-type xml/json not specified in response headers please.\n'
            'Set return_response_ez=True to work directly with the response object.'
        )
    return do_request


# FINAL SETUP FOR RestEast
for method in RestEasy.HTTP_METHODS:
    setattr(RestEasy, method.upper(), bind_method(method))
