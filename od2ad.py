from attrdict import AttrDict
from collections import OrderedDict

test = OrderedDict({
    'test': OrderedDict({
        'nested':OrderedDict({
            'one':1,
            'two':2,
        })
    })
})

print(test)

def convert(d):
    d = AttrDict(d)
    for k,v in d.items():
        if isinstance(v, OrderedDict):
            d[k] = convert(v)
    return d

print(convert(test))